'''
Created on Mar 2, 2015

Implementation of modules for the transfer of files between
the local nersc systems and alien or hpss. A factory automatically
detrmines the correct handler for a given process

@author: markus
'''

from Datatransfer.PyAlien.AlienTools import alien_copy_file, AlienException,\
    alien_checksum, alien_ls, alien_remove_file
import os, hashlib

class CopyHandlerFactory(object):
    '''
    Factory class, handles access to transfer handler for the users
    '''
    
    @staticmethod
    def GetCopyHandler(sourcefile, targetfile):
        '''
        Determines the type of transfer handlers based on tags in the location names
        '''
        if "alien" in sourcefile or "alien" in targetfile:
            return CopyHandlerAlien(sourcefile, targetfile)
        elif "hpss" in sourcefile or "hpss" in targetfile:
            return CopyHandlerHPSS(sourcefile, targetfile)
        
    @staticmethod
    def DoCopy(sourcefile, targetfile, workerparams):
        '''
        Interface for copys: Determins the type of copy handler and performs
        the copy
        '''
        handler = CopyHandlerFactory.GetCopyHandler(sourcefile, targetfile)
        handler.SetWorkerParams(workerparams)
        handler.PerformCopy()
        return handler.GetTransferStatus()

class CopyHandler(object):
    '''
    Base class for copy handling modules in the transfer framework
    '''

    def __init__(self, sourcefile, targetfile):
        '''
        Constructor
        '''
        self._sourcelocation = sourcefile.lstrip().rstrip()
        self._targetlocation = targetfile.lstrip().rstrip()
        self._transferstatus = False
        self._workerparams = None
       
    def PerformCopy(self):
        print "Method to be implemented by inheriting class"
        
    def SetWorkerParams(self, wparams):
        """
        Set worker params to the 
        """
        self._workerparams = wparams
        
    def GetTransferStatus(self):
        '''
        Get the status of the transfer
        '''
        return self._transferstatus
    
    def GetFileChecksum(self, filename):
        """
        Get the checksum of a file
        """
        pass
    
    def _GetLocalChecksum(self, filename):
        """
        Get the md5sum of a local file
        """
        return hashlib.md5(open(filename, 'rb').read()).hexdigest()        
    
class CopyHandlerAlien(CopyHandler):
    '''
    Copy Handler implementation for transfers between alien and nersc
    '''
    
    def __init__(self, sourcelocation, targetlocation):
        '''
        Constructor
        '''
        CopyHandler.__init__(self, sourcelocation, targetlocation)
        self.__token = None
        
    def PerformCopy(self):
        '''
        Method performing copy between nersc and alien
        works in both directions. 
        After copy is done, some basic tests are performed
        1. Is the targetfile existing
        2. Does the checksum of the sourcefile and the targetfile match
        '''
        if not self.__CheckTokenValidity():
            return 
        if "alien://" in self._sourcelocation:
            # prepare file system structure
            dirname = os.path.dirname(self._targetlocation)
            if not os.path.exists(dirname):
                try:
                    os.makedirs(dirname, 0755)
                except Exception as e:
                    print "Failed creating output directory: %s" %(str(e))
                    return
        try:
            alien_copy_file(self._sourcelocation, self._targetlocation)
            # check if the file is there
            if not "alien://" in self._targetlocation:
                if os.path.exists(self._targetlocation):
                    # check whether checksums match
                    checksumSource = self.GetFileChecksum(self._sourcelocation)
                    checksumTarget = self.GetFileChecksum(self._targetlocation)
                    if  checksumSource == checksumTarget:
                        self._transferstatus = True
                    else:
                        # checksums differ - mark file as bad
                        print "Checksums differ: Source[%s], Target[%s]" %(checksumSource, checksumTarget)
                        os.remove(self._targetlocation)
                else:
                    print "Targetlocation %s not existing" %(self._targetlocation)
            else:
                # copy to alien
                workfile = self._targetlocation
                workfile = workfile.replace("alien://","")
                alienlocation = ""
                try:
                    alienlocation = alien_ls(workfile)
                except AlienException:
                    self._transferstatus = False
                    return
                if len(alienlocation):
                    # file there - compare checksums
                    if self.GetFileChecksum(self._sourcelocation) == self.GetFileChecksum(self._targetlocation):
                        self._transferstatus = True
                    else:
                        # checksums differ - mark file as bad
                        alien_remove_file(self._targetlocation)
                        self._transferstatus = False
                else:
                    self._transferstatus = False
        except AlienException:
            self._transferstatus = False 
            
    def GetFileChecksum(self, filename):
        """
        Get the checksum of a file
        """
        if "alien://" in filename:
            return self._GetAlienChecksum(filename)
        return self._GetLocalChecksum(filename)

    def _GetAlienChecksum(self, filename):
        """
        Get the checksum from a file in alien
        """
        if not self.__CheckTokenValidity():
            return ""
        result = ""
        try:
            result = alien_checksum(filename)
        except AlienException as e:
            print "Cannot get he checksum: %s" %(str(e))
        return result

    def __CheckTokenValidity(self):
        """
        Check if the current token used under this machine is still valid
        Token information is steered by the worker params
        """
        if not self._workerparams:
            return False
        
        token = self._workerparams.GetAlienToken()
        if not token:
            return False
        
        return token.IsValid()
    
class CopyHandlerHPSS(CopyHandler):
    '''
    Copy Handler implementation for transfers between hpss and local disks
    '''
    
    def __init__(self, sourcelocation, targetlocation):
        '''
        Constructor
        '''
        CopyHandler.__init__(self, sourcelocation, targetlocation) 
        
    def PerformCopy(self):
        '''
        Method performing transfers between HPSS and local disks
        '''
        pass