'''
Created on Mar 4, 2015

Helper class storing worker (node) specific settings
Distributed via the thread manager

@author: markus
'''
from Datatransfer.PyAlien.AlienTokenReader import AlienTokenReader
from datetime import datetime, timedelta
import threading

class WorkerSettings(object):
    '''
    Class keeping worker-specific settings
    '''
    lockhandler = threading.Lock()

    def __init__(self):
        '''
        Constructor
        '''
        self.__alienToken = None
        self.__lastTokenUpdate = None
        
    def GetAlienToken(self):
        """
        Get alien token, update if we are beyond the update interval
        Put in lock to be thread-safe
        """
        WorkerSettings.lockhandler.acquire()
        if not self.__alienToken:
            self.UpdateTokenInfo()
        # check whether we need to update
        if datetime.now() > self.__lastTokenUpdate + timedelta(minutes=15):
            self.UpdateTokenInfo()
        WorkerSettings.lockhandler.release()
        return self.__alienToken
    
    def UpdateTokenInfo(self):
        """
        Update token info
        """
        try:
            reader = AlienTokenReader()
            reader.CreateTokenInfo()
            self.__alienToken = reader.GetToken()
            self.__lastTokenUpdate = datetime.now()
        except Exception as e:
            print "Reading token failed: %s" %(str(e))
            self.__alienToken = None
            self.__lastTokenUpdate = None       