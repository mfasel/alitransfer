'''
Created on 11.09.2014

@author: markusfasel
'''
from commands import getstatusoutput
import pymongo
import simplejson
import time, os

class QueueEntry:
    def __init__(self):
        self.__uid = None
        self.__owner = None
        self.__inputpath = None
        self.__outputpath = None
        self.__status = "Q"
        self.__ntry = 0
        self.__transfertime = 0
        
    def SetOwner(self, owner):
        self.__owner = owner
        
    def GetOwner(self):
        return self.__owner
    
    def SetUID(self, uid):
        self.__uid = uid
        
    def GetUID(self):
        return self.__uid
    
    def SetInputPath(self, path):
        self.__inputpath = path
        
    def SetOutputPath(self, path):
        self.__outputpath = path
        
    def GetInputPath(self):
        return self.__inputpath
    
    def GetOutputPath(self):
        return self.__outputpath
    
    def IncrementTry(self):
        self.__ntry += 1
        
    def GetNumberOfTries(self):
        return self.__ntry
    
    def SetTransferTime(self, ttime):
        self.__transfertime = ttime
        
    def GetTransferTime(self):
        return self.__transfertime
    
    def JSONString(self):
        encoder = simplejson.JSONEncoder()
        return encoder.encode(self)
    
    @staticmethod
    def BuildFromJSON(jsonstring):
        decoder = simplejson.JSONDecoder()
        decoder.decode(jsonstring)
        result.SetInputPath(values["inputpath"])
    
class QueueDatabaseConnector:
    
    def __init__(self, url):
        self.__dbclient = pymongo.MongoClient(url)
    
    def AddEntryToDatabase(self, entry):
        pass
    
    def UpdateEntry(self, entry):
        pass
    
    def GetNextInQueue(self):
        pass
    
    def GetEntries(self, jsonquery):
        pass
    
    def GetDone(self, owner = None):
        pass
    
    def GetQueued(self, owner = None):
        pass
    
    def GetRunning(self, owner = None):
        pass
    
    def GetError(self):
    
    def GetOwner(self, owner):
        pass

class TransferQueue:
    '''
    Queue implementation for data transfer using alien
    '''

    def __init__(self, dburl):
        '''
        Constructor
        '''
        self.__dbconnection = QueueDatabaseConnector(dburl)
            
    def GetNextFromQueue(self):
        '''
        Get the next file in the queue
        '''
        pass
    
    def TransferFile(self, entry):
        '''
        Transfer file specified in the entry
        '''
        self.UpdateStatus(entry, "R")
        #build transport command
        commandstring = "source /usr/share/Modules/init/bash;"
        commandstring += "module use /project/projectdirs/alice/software/modulefiles/;"
        commandstring += "module load alice/alien/default;"
        commandstring += "source /tmp/gclient_env_$UID;"
        commandstring += "alien_cp %s %s;" %(entry.GetInputPath(), entry.GetOutputPath())
        starttime = time.time()
        output = getstatusoutput(commandstring)
        endtime = time.time()
        entry.SetTransferTime(endtime - starttime)
        # check output for errors and requeue if necessary
        if "Error" in output:
            if os.path.exists(entry.GetOutputPath):
                os.remove(entry.GetOutputPath())
            self.Requeue(entry)
        if not os.path.exists(entry.GetOutputPath()):
            self.Requeue(entry)
    
    def MarkFileAsDone(self, entry):
        '''
        After transfer is done, mark file as advanced
        '''
        self.UpdateStatus(entry, "D")
        self.__dbconnection.UpdateEntry(entry)
    
    def Requeue(self, entry):
        '''
        Put entry back to the queue
        '''
        self.UpdateStatus(entry, "Q")
        entry.IncrementTry()
        self.__dbconnection.UpdateEntry(entry)
    
    def UpdateStatus(self, entry, status):
        '''
        Change status of a given entry
        '''
        entry.SetStatus(status)
        
    def RunDaemon(self):
        '''
        Run transfer Daemon
        '''
        pass